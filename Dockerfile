FROM python:3.6-alpine as base
RUN apk update && apk add build-base

RUN mkdir /app
WORKDIR /app
COPY . /app

RUN pip install --no-cache-dir -r requirements.txt

FROM python:3.6-alpine

COPY --from=base /app/ /app
COPY --from=base /usr/local/lib/python3.6 /usr/local/lib/python3.6
WORKDIR /app

EXPOSE 5000
ENTRYPOINT [ "python" ]
CMD [ "app.py" ]